///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Kayla Valera <kvalera@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   @todo 04_MAY_2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

bool CatEmpire::empty() {
	   if (topCat == nullptr) {
		         return true;
			    }

	      return false;
}

void CatEmpire::addCat( Cat* newCat ) {
	   newCat->left = nullptr;
	      newCat->right = nullptr;
	         assert( newCat != nullptr);

		    if (topCat == nullptr) {
			          topCat = newCat;
				        return;
					   }
		       
		       addCat(topCat,newCat);
}

void CatEmpire::addCat( Cat* atCat, Cat* newCat ){
	   assert(atCat != nullptr);
	      assert(newCat != nullptr);
 if ( atCat->name > newCat->name ) {
	if( atCat ->left == nullptr ) {
	atCat->left = newCat;
	} else {
	addCat( atCat->left, newCat );
						             }
	          }
 if( atCat->name < newCat->name ) {
	  if ( atCat->right == nullptr ) {
 atCat->right = newCat;
       } else {
	 addCat( atCat->right, newCat );
			      }
	     }
}

void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
	cout << "No cats!" << endl;
	return;
	}
	dfsInorderReverse( topCat, 1 );
}

void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const{
	   if(atCat == nullptr)
		         return;
	   dfsInorderReverse(atCat->right, (depth+1));

	      cout << string(6*(depth-1), ' ') << atCat->name;
if ( atCat->left != nullptr && atCat->right != nullptr ) {
            cout << "<" << endl;
       } else if(atCat->left != nullptr && atCat->right == nullptr) {
         cout << "\\" << endl;
   } else if(atCat->left == nullptr && atCat->right != nullptr) {
        cout << "/" << endl;
   } else {
         cout << endl;
   }

   dfsInorderReverse(atCat->left, (depth+1));

 }

void CatEmpire::catList() const {
	   if( topCat == nullptr ) {
	cout << "No cats!" << endl;
	return;
	}

	 dfsInorder( topCat );
}

void CatEmpire::dfsInorder( Cat* atCat ) const{
	   if ( atCat == nullptr ) {
		         return;
			    }
	      dfsInorder(atCat->left);
	         cout << atCat->name << endl;

		    dfsInorder(atCat->right);

}

void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
	cout << "No cats!" << endl;
	return;
	}

	dfsPreorder( topCat );
}

void CatEmpire::dfsPreorder( Cat* atCat ) const{
	   if ( atCat == nullptr ) {
		         return;
			    }
	      if ( atCat->left != nullptr && atCat->right != nullptr ) {
	cout << atCat->name << " begat " << atCat->left->name << " and " << atCat->right->name << endl;
	} else if ( atCat->left != nullptr && atCat->right == nullptr ) {
	cout << atCat->name << " begat " << atCat->left->name << endl;
	} else if ( atCat->left == nullptr && atCat->right != nullptr ) {
	cout << atCat->name << " begat " << atCat->right->name << endl;
	}

	dfsPreorder(atCat->left);
	dfsPreorder(atCat->right);
}

void CatEmpire::getEnglishSuffix(  int n ) const{
	   if(n%10 == 1 && n != 11) {
		         cout << n << "st ";
			    }
	      else if(n%10 == 2 && n != 12) {
		            cout << n << "nd ";
			       }
	         else if(n%10 == 3 && n != 13) {
			       cout << n << "rd ";
			          }
		    else { 
			          cout << n << "th ";
				     }
}

void CatEmpire::catGenerations() const{
	   int generation = 1;
	   int next = 0;
	   int now = 1;

	getEnglishSuffix(generation);
	cout << "Generation" << endl << "   ";
	queue<Cat*> catQueue;
	catQueue.push(topCat);

	while(!catQueue.empty()){
	Cat* cat = catQueue.front();
	catQueue.pop();

	if(now == 0){
	generation++;
	cout<<endl;
										             getEnglishSuffix(generation);
											              cout << "Generation" << endl << "   ";
												               now = next;
													                next = 0;
															      }

 if(cat == nullptr) {
          return;
       }
      
      if(cat->left != nullptr){
	  catQueue.push(cat->left);
		              next++;
			           }

           if(cat->right != nullptr){
		           catQueue.push(cat->right);
			           next++;
				        }

	        cout << cat->name << "  ";
	             now--;
	        }
   cout << endl;
}


Cat::Cat( const std::string newName ) {
		setName( newName );
}


void Cat::setName( const std::string newName ) {
		assert( newName.length() > 0 );

			name = newName;
}


std::vector<std::string> Cat::names;

void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}

Cat* Cat::makeCat() {

	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];
erase( names, names[nameIndex] );

	return new Cat( name );
}


	
	
	
	
	   //from slides
		 //   if( atCat->name > newCat->name){
		 //         if(atCat->left == nullptr){
		 //                     atCat->left = newCat;
		 //                           }
		 //                                 else {
		 //                                          addCat( atCat->left, newCat);
		 //                                                }
		 //                                                   }
		 //
		 //                                                      if(atCat->name < newCat->name) {
		 //                                                            if( atCat->right == nullptr){
		 //                                                                     atCat->right = newCat;
		 //                                                                           }else{
		 //                                                                                    addCat(atCat->right, newCat);
		 //                                                                                          }
		 //                                                                                             }
		 //                                                                                             };
		 //
		 //                                                                                             void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const{
		 //                                                                                                
		 //                                                                                                   //if at null
		 //                                                                                                      if(atCat == nullptr){
		 //                                                                                                            return;
		 //                                                                                                               }
		 //
		 //                                                                                                                  if( atCat->right = nullptr){
		 //                                                                                                                        dfsInorderReverse(atCat->right, depth+1); //changes depth as no more exist at this level
		 //                                                                                                                           }
		 //
		 //                                                                                                                              cout<< string(6*(depth-1), ' ') << atCat->name; //prints name with 6 spaces * depth
		 //
		 //                                                                                                                                 if(atCat->left == nullptr && atCat->right == nullptr){
		 //                                                                                                                                       cout<<endl; //defined in lab manual
		 //                                                                                                                                          }
		 //                                                                                                                                             
		 //                                                                                                                                                if(atCat->left != nullptr && atCat->right == nullptr){
		 //                                                                                                                                                      cout<< "\\" <<endl;
		 //                                                                                                                                                         }
		 //
		 //                                                                                                                                                            if(atCat->left != nullptr && atCat->right != nullptr){ //meaning we are not at end
		 //                                                                                                                                                                  cout << "<" << endl;
		 //                                                                                                                                                                     }
		 //                                                                                                                                                                        
		 //                                                                                                                                                                           if(atCat->left == nullptr && atCat->right != nullptr){
		 //                                                                                                                                                                                 cout<<"/" <<endl;
		 //                                                                                                                                                                                    }
		 //
		 //                                                                                                                                                                                       if(atCat->left != nullptr){
		 //                                                                                                                                                                                             dfsInorderReverse(atCat->left, depth+1);
		 //                                                                                                                                                                                                }
		 //                                                                                                                                                                                                   
		 //                                                                                                                                                                                                   }
		 //
		 //                                                                                                                                                                                                   void CatEmpire::dfsInorder( Cat* atCat) const{
		 //                                                                                                                                                                                                      if(atCat == nullptr){
		 //                                                                                                                                                                                                            return;}
		 //
		 //                                                                                                                                                                                                               dfsInorder( atCat->left);
		 //                                                                                                                                                                                                                  cout<<atCat->name<<endl;
		 //                                                                                                                                                                                                                     dfsInorder(atCat->right);
		 //
		 //                                                                                                                                                                                                                     }
		 //
		 //                                                                                                                                                                                                                     void CatEmpire::dfsPreorder( Cat* atCat) const{
		 //                                                                                                                                                                                                                        if(atCat == nullptr){
		 //                                                                                                                                                                                                                              return;}
		 //
		 //                                                                                                                                                                                                                                 if(atCat->left != nullptr && atCat->right != nullptr){
		 //                                                                                                                                                                                                                                       cout<<atCat->name<<"begat"<<atCat->left->name<<"and"<<atCat->right->name<<endl;
		 //                                                                                                                                                                                                                                          }
		 //                                                                                                                                                                                                                                             if(atCat->left != nullptr && atCat->right == nullptr){ //if no left cat, print center and right
		 //                                                                                                                                                                                                                                                   cout<<atCat->name<<"begat"<<atCat->left->name<<endl;
		 //                                                                                                                                                                                                                                                      }
		 //                                                                                                                                                                                                                                                         if(atCat->left == nullptr && atCat->right != nullptr){ //if no right cat, print left and center
		 //                                                                                                                                                                                                                                                               cout<<atCat->right->name<<"begat"<<atCat->name<<endl;
		 //                                                                                                                                                                                                                                                                  }
		 //                                                                                                                                                                                                                                                                     
		 //                                                                                                                                                                                                                                                                        if(atCat->left == nullptr && atCat->right == nullptr){ //no cats left
		 //                                                                                                                                                                                                                                                                              return;
		 //                                                                                                                                                                                                                                                                                 }
		 //
		 //                                                                                                                                                                                                                                                                                    dfsPreorder(atCat->left);
		 //                                                                                                                                                                                                                                                                                       dfsPreorder(atCat->right);
		 //
		 //                                                                                                                                                                                                                                                                                       }
		 //
		 //                                                                                                                                                                                                                                                                                       void CatEmpire::getEnglishSuffix(int n) const{
		 //                                                                                                                                                                                                                                                                                             // 1"st" 2"nd" 3"rd" 4"th" 
		 //
		 //                                                                                                                                                                                                                                                                                                   //special 11th, 12th, 13th (hundred)
		 //                                                                                                                                                                                                                                                                                                         if(n <10){
		 //                                                                                                                                                                                                                                                                                                                     if(n == 1){
		 //                                                                                                                                                                                                                                                                                                                                    cout<<n<<"st"<<endl;
		 //                                                                                                                                                                                                                                                                                                                                                }
		 //                                                                                                                                                                                                                                                                                                                                                            else if(n == 2){
		 //                                                                                                                                                                                                                                                                                                                                                                           cout<<n<<"nd"<<endl;
		 //                                                                                                                                                                                                                                                                                                                                                                                       }
		 //                                                                                                                                                                                                                                                                                                                                                                                                   else if(n == 3){
		 //                                                                                                                                                                                                                                                                                                                                                                                                                  cout<<n<<"rd"<<endl;
		 //                                                                                                                                                                                                                                                                                                                                                                                                                              }
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                          else{
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                         cout<<n<<"th"<<endl;
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                     }
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                           }
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 if(n<20 && n>10){
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                cout<<n<<"th"<<endl;
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      }
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            if(n>20){
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    int x = n%10;
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              if(x==1){
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             cout<<n<<"st"<<endl;
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         }
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     else if(x==2){
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    cout<<n<<"nd"<<endl;
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                }
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            else if(x==3){
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           cout<<n<<"rd"<<endl;
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       }
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   else{
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  cout<<n<<"th"<<endl;
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              }
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    }
		 //
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    }
		 //
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    void CatEmpire::bfs(){
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       queue<Cat*> catQueue;
		 //
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          catQueue.push(topCat);
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             while(catQueue.empty() != 1){
		 //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        




