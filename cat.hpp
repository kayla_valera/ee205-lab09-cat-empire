///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.hpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Kayla Valera <kvalera@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   @todo 05_MAY_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <iostream>

class Cat {
protected: 
	Cat* left = nullptr;
	Cat* right = nullptr;


private:    /// Member variables
	std::string name;

public:     /// Constructors
	Cat( const std::string newName );
	friend class CatEmpire;

private:    /// Private methods
	void setName( const std::string newName ); // Name is a key in our BST, so we don't
		                                        // want to let people change it as it'll
		                                        // mess up the tree.

private:    /// Static variables
	static std::vector<std::string> names;

public:     /// Static methods
	static void initNames();
	static Cat* makeCat();
};


class CatEmpire {
private:
	Cat* topCat = nullptr;

public:
	bool empty();  // Return true if empty
   void getEnglishSuffix( int n ) const;


public:
	void addCat( Cat* newCat );  // Add a cat starting at the root

	void catFamilyTree() const;
	void catList() const;
	void catBegat() const;
	void catGenerations() const;

private:
	void addCat( Cat* atCat, Cat* newCat );  // Add a cat starting at atCat

	void dfsInorderReverse( Cat* atCat, int depth ) const;
	void dfsInorder( Cat* atCat ) const;
	void dfsPreorder( Cat* atCat ) const;
};
